---
layout: page
menu: false
date: '2020-04-19 03:52:25'
title: About
permalink: /about/
description: Do you know you are being watched by big tech companies every single day? Do you know that advertisement used your cookies to serve you your personal ads. Wonder how they get it from? This blog will shoot a cannon to their practices.
---

<img class="img-rounded" src="/assets/img/uploads/profile.png" alt="Thiago Rossener" width="200">

# About

You are being watched.
You are asking yourself, "What? They said they trust my data!"
But think of like this:
- Do you have proof they don't get your data?
- How does your advertisement knew what you might want?
- How do you check if your software claims it doesn't spy on you?
- How does Google offer free services?

There are a lot of questions like these.
If you cannot answer them, well, you are being sold.
If you want to get your data back in control, well, you are in the right place.